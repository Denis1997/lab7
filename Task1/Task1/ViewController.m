//
//  ViewController.m
//  Task1
//
//  Created by fpmi on 20.04.16.
//  Copyright (c) 2016 fpmi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSArray *_pickerData;
}

@property (weak, nonatomic) IBOutlet UITextField *widthText;
@property (weak, nonatomic) IBOutlet UIImageView *canvas;
@property CGPoint lastPoint;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;

@end

@implementation ViewController

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [self setLastPoint:[touch locationInView:self.view]];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *) event
{
    UITouch *touch = [touches anyObject];
    CGPoint currentPoint = [touch locationInView:self.view];
    UIGraphicsBeginImageContext(self.view.frame.size);
    
    CGRect drawRect = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height);
    
    [[[self canvas] image] drawInRect:drawRect];
    
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    
    float lineWidth = 5.0f;
    lineWidth = [[self widthText].text floatValue];
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), lineWidth);
    float r, g, b, a;
    a = 1.0f;
    
    NSInteger row = [[self picker] selectedRowInComponent:0];
    if (row == 0)
    {
        r = 1.0f;
        g = 0.0f;
        b = 0.0f;
    }
    else
    if (row == 1)
    {
        r = 0.0;
        g = 1.0f;
        b = 0.0;
    }
    else
    if (row == 2)
    {
        r = 0.0f;
        g = 0.0f;
        b = 1.0f;
    }
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), r, g, b, a);
    CGContextBeginPath(UIGraphicsGetCurrentContext());
    
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), _lastPoint.x, _lastPoint.y);
    
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    [[self canvas] setImage:UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    
    _lastPoint = currentPoint;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    _pickerData = @[@"Red", @"Green", @"Blue"];
    
    self.picker.dataSource = self;
    self.picker.delegate = self;
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _pickerData.count;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _pickerData[row];
}

@end
